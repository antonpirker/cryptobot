# cryptobot



### Creating a python3 virtualenv on webfaction:
```
mkdir /home/ignaz/.venvs
python3 -m venv /home/ignaz/.venvs/cryptobot
```


### Activating the virtualenv
```
$ . /home/ignaz/.venvs/cryptobot/bin/activate
```